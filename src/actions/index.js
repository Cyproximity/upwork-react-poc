import fetch from "cross-fetch";
import * as _ from "lodash";
import moment from "moment";

export const REQUEST_EMPLOYEES = "REQUEST_EMPLOYEES";
export const REQUEST_SHIFTS = "REQUEST_SHIFTS";
export const GROUP_SHIFTS = "GROUP_SHIFTS";
export const LOADED_SHIFTS = "LOADED_SHIFTS";
export const UPDATE_EMPLOYEE_SHIFT = "UPDATE_EMPLOYEE_SHIFT";
export const TOGGLE_SHIFT_UPDATE = "TOGGLE_SHIFT_UPDATE";
export const REQUEST_ROLES = "REQUEST_ROLES";
export const SORT_ROLES = "SORT_ROLES";
export const REQUEST_CONFIG = "REQUEST_CONFIG";

export function fetchEmployees() {
  return dispatch => {
    return fetch(`api/employees`)
      .then(response => response.json())
      .then(json => dispatch({ type: REQUEST_EMPLOYEES, payload: json }));
  };
}

export function fetchRoles() {
  return dispatch => {
    return fetch(`api/roles`)
      .then(response => response.json())
      .then(json => dispatch({ type: REQUEST_ROLES, payload: json }));
  };
}

export function fetchShifts(roles, tz) {
  return dispatch => {
    dispatch({ type: LOADED_SHIFTS, payload: false });

    return fetch(`api/shifts`)
      .then(response => response.json())
      .then(async json => {
        await dispatch({
          type: GROUP_SHIFTS,
          payload: groupShifts(json, tz)
        });

        await dispatch({
          type: REQUEST_SHIFTS,
          payload: sortShirftByRoles(json, roles, tz)
        });

        await dispatch({
          type: LOADED_SHIFTS,
          payload: true
        });
      });
  };
}

export function updateGroupShifts(list, tz) {
  return dispatch =>
    dispatch({
      type: GROUP_SHIFTS,
      payload: groupShifts(list, tz)
    });
}

export function updateEmployeeShift(id, startTime, endTime, breakTime, tz) {
  return dispatch => {
    dispatch({ type: TOGGLE_SHIFT_UPDATE, payload: true });
    setTimeout(
      () =>
        dispatch({
          type: UPDATE_EMPLOYEE_SHIFT,
          payload: {
            id,
            start_time: moment.tz(startTime, tz),
            end_time: moment.tz(endTime, tz),
            break_duration: breakTime
          }
        }),
      300
    );
  };
}

export function fetchConfig() {
  return dispatch => {
    return fetch(`api/config`)
      .then(response => response.json())
      .then(json => dispatch({ type: REQUEST_CONFIG, payload: json }));
  };
}

// PRIVATE

function groupShifts(shifts, tz) {
  let group = _.groupBy(shifts, date => {
    const timezone = moment.tz(date.end_time, tz);
    return timezone.startOf("day").format();
  });

  return group;
}

function sortShirftByRoles(list, roles, tz) {
  return _.map(list, employee => {
    const assignEmployeeRole = roles.find(role => role.id === employee.role_id);

    return {
      ...employee,
      group: employee.employee_id,
      title: assignEmployeeRole.name,
      start_time: moment.tz(employee.start_time, tz),
      end_time: moment.tz(employee.end_time, tz),
      className: assignEmployeeRole.name,
      style: {
        backgroundColor: `${assignEmployeeRole.background_colour}`
      }
    };
  });
}
