import { combineReducers } from "redux";

import {
  REQUEST_EMPLOYEES,
  REQUEST_SHIFTS,
  LOADED_SHIFTS,
  TOGGLE_SHIFT_UPDATE,
  UPDATE_EMPLOYEE_SHIFT,
  REQUEST_ROLES,
  REQUEST_CONFIG,
  GROUP_SHIFTS
} from "../actions";

function employees(state = [], action) {
  switch (action.type) {
    case REQUEST_EMPLOYEES: {
      const addGroup = action.payload.map(x => ({
        ...x,
        group: x.id,
        title: `${x.last_name}, ${x.first_name}`
      }));

      return (state = addGroup);
    }
    default:
      return state;
  }
}

const shiftsInitState = {
  isUpdated: false,
  isLoaded: false,
  shiftsList: [],
  groupByDate: {}
};

function shifts(state = shiftsInitState, action) {
  switch (action.type) {
    case REQUEST_SHIFTS: {
      return {
        ...state,
        shiftsList: action.payload
      };
    }

    case GROUP_SHIFTS: {
      return {
        ...state,
        groupByDate: action.payload
      };
    }

    case TOGGLE_SHIFT_UPDATE: {
      return { ...state, isUpdated: action.payload ? true : false };
    }

    case UPDATE_EMPLOYEE_SHIFT: {
      let { shiftsList } = state;
      const { id, start_time, end_time, break_duration } = action.payload;
      const locatedShift = shiftsList.findIndex(shift => shift.id === id);

      const updatedShift = {
        ...shiftsList[locatedShift],
        start_time,
        end_time,
        break_duration
      };

      shiftsList[locatedShift] = updatedShift;

      return { ...state, shiftsList: shiftsList, isUpdated: false };
    }

    case LOADED_SHIFTS: {
      return {
        ...state,
        isLoaded: action.payload ? true : false
      };
    }

    default:
      return state;
  }
}

function roles(state = [], action) {
  switch (action.type) {
    case REQUEST_ROLES: {
      return (state = action.payload);
    }
    default:
      return state;
  }
}

const configInitState = {
  timezone: "",
  location: ""
};

function config(state = configInitState, action) {
  switch (action.type) {
    case REQUEST_CONFIG: {
      const { payload } = action;
      return Object.assign({}, state, payload);
    }
    default:
      return state;
  }
}

const reducer = combineReducers({
  employees,
  shifts,
  roles,
  config
});

export default reducer;
