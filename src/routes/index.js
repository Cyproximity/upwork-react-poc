import React from "react";
import { Switch, Route, NavLink } from "react-router-dom";
import { Redirect } from "react-router";

import { Layout, Menu, Icon, Typography } from "antd";
import routesHoc from "./routesHoc";
import Timeline from "./timeline";
import Feed from "./feed";
import NotFound from "./404";

const { Sider, Footer } = Layout;
const { Title } = Typography;

class InitialRoute extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: false
    };
  }

  componentDidMount() {}

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    if (this.props.location.pathname === "/") {
      return <Redirect to={`/timeline`} />;
    }

    return (
      <Layout>
        <Sider
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={broken => {}}
          onCollapse={(collapsed, type) => {}}
        >
          <Title level={4} className="logo">
            Employee Shifts
          </Title>
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={[`${this.props.location.pathname}`]}
            selectedKeys={[`${this.props.location.pathname}`]}
          >
            <Menu.Item key={`/timeline`}>
              <NavLink to={`/timeline`} className="nav-text">
                <Icon type="calendar" />
                <span>Calendar Timeline</span>
              </NavLink>
            </Menu.Item>
            <Menu.Item key={`/feed`}>
              <NavLink to={`/feed`} className="nav-text">
                <Icon type="database" />
                <span>Feed</span>
              </NavLink>
            </Menu.Item>
            {/* <Menu.Item key={`/employees`}>
              <NavLink to={`/employees`} className="nav-text">
                <Icon type="user" />
                <span className="nav-text">Employees</span>
              </NavLink>
            </Menu.Item> */}
          </Menu>
        </Sider>
        <Layout>
          <Switch>
            <Route path="/timeline" exact component={Timeline} />
            <Route path="/feed" exact component={Feed} />
            {/* <Route path="/employees" exact component={Timeline} /> */}
            <Route component={NotFound} />
          </Switch>
          <Footer style={{ textAlign: "center" }}>...</Footer>
        </Layout>
      </Layout>
    );
  }
}

export default routesHoc(InitialRoute);
