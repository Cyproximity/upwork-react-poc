import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import InitialRoute from "./routes";
import configureStore from "./configureStore";

import "./App.css";

const store = configureStore();

const Routes = () => (
  <Router>
    <Route path="/" component={InitialRoute} />
  </Router>
);

function App() {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
}

export default App;
